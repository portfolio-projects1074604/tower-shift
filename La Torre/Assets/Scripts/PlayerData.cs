using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData
{
    public int health;
    public int max_health;
    public float[] position;

    public string save_room;

    public PlayerData(PlayerController player)
    {
        health = player.health;
        max_health = player.max_health;

        position = new float[2];

        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;

        save_room = player.last_SR;
    }
}
