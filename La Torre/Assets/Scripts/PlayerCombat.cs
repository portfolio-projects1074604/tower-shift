using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Animator animator;

    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;

    private int damage = 20;
    private int damage_mod = 1;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("Attack");
        }
    }

    public void Attack()
    {
        Debug.Log("Ataco");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
        foreach(Collider2D enemy in hitEnemies)
        {
            if (enemy.GetComponent<MechaGolem>())
            {
                enemy.GetComponent<MechaGolem>().TakeDamage(damage + damage_mod);
            }
            if (enemy.GetComponent<EnemyAI>())
            {
                enemy.GetComponent<EnemyAI>().TakeDamage(damage + damage_mod);
            }
            if (enemy.GetComponent<Spider>())
            {
                enemy.GetComponent<Spider>().TakeDamage(damage + damage_mod);
            }
            if (enemy.GetComponent<Bat>())
            {
                enemy.GetComponent<Bat>().TakeDamage(damage + damage_mod);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint != null)
        {
            Gizmos.DrawWireSphere(attackPoint.position, attackRange);
        } 
    }
}
