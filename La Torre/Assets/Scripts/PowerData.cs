using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Levels/Data")]
public class PowerData : ScriptableObject
{
    public int health;
    public int max_health;
    public HashSet<string> powers = new HashSet<string>();
}
