using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    [SerializeField] private PowerData data;

    public void LoadGame()
    {
        PlayerData player = SaveSystem.LoadPlayer();
        data.health = player.health;
        data.max_health = player.max_health;
        SceneManager.LoadScene(player.save_room);
    }

    public void NewGame()
    {
        data.max_health = 100;
        data.health = data.max_health;
        data.powers.Clear();
        SceneManager.LoadScene("Room 1");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
