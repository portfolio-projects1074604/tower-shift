using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Animator player_animator;
    public CharacterController2D controller;                            //Referencia al controlador 2D del jugador
    public Rigidbody2D rb;                                              //Referencia al componente RigidBody2D del player
    public float speed;                                                 //Valor de la velocidad de movimiento del jugador    
    public int health;
    public int max_health;
    public int base_damage;
    public int damage_mod = 1;

    public Healthbar healthbar;
    public GameObject p_menu;

    public int energy;
    public int max_energy;

    public bool inWater = false;
    private bool _isInvincible = false;
    private bool jump;                                                  //Booleano que controla si podemos o no saltar

    public PowerData data;
    public string last_SR;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        player_animator = GetComponent<Animator>();

        p_menu = GameObject.Find("PauseMenu");
        p_menu.SetActive(false);

        healthbar = GameObject.Find("Healthbar").GetComponent<Healthbar>();

        if (healthbar != null)
        {
            healthbar.SetMaxHealth(data.health, data.max_health);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (data.health <= 0)
        {
            SceneManager.LoadScene("MainMenu");
            Destroy(this.gameObject);
        }
        if (!p_menu.activeSelf)
        {
            //Si pulsamos el boton de salto habilitamos el salto
            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
                player_animator.SetTrigger("Jump");
            }
            //Si pulsamos el boton de invertir la gravedad volvemos la escala de gravedad negativa y llamamos a Invert()
            if (Input.GetButtonDown("Invert") && data.powers.Contains("GRAVITY") && !inWater)
            {
                rb.gravityScale *= -1;
                controller.Invert();
            }
            if (Input.GetButtonDown("Menu"))
            {
                p_menu.gameObject.SetActive(true);
                this.enabled = false;
            }
        }
        health = data.health;
        max_health = data.max_health;
    }

    private void FixedUpdate()
    {
        //Aplicamos el movimiento correspondiente
        controller.Move(Input.GetAxis("Horizontal") * speed * Time.fixedDeltaTime, false, jump);
        player_animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        jump = false;
    }

    public void TakeDamage(int damage)
    {
        if (!_isInvincible)
        {
            data.health -= damage;
            healthbar.SetHealth(data.health);
            StartCoroutine(IFrames());
        }
    }

    public void LvUpDamage()
    {
        damage_mod += 1;
    }

    public void LvUpHP()
    {
        data.max_health += 100;
        data.health = max_health;
    }

    public void AddPower(string power)
    {
        data.powers.Add(power);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "HealthDrop":
                health += 20;
                if (health > max_health) health = max_health;
                break;
            case "Energy>Drop":
                energy += 20;
                if (energy > max_energy) energy = max_energy;
                break;
            case "HealthTank":
                max_health += 100;
                break;
            case "EnergyTank":
                max_energy += 100;
                break;
            default:
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Water"))
        {
            inWater = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Water"))
        {
            inWater = false;
        }
    }

    public IEnumerator IFrames()
    {
        _isInvincible = true;
        yield return new WaitForSeconds(2f);
        _isInvincible = false;
    }
}
