using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    private static GameMaster instance;
    //[SerializeField] private static GameObject player;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
        /*if (player == null)
        {
            player = GameObject.Find("Player");
            DontDestroyOnLoad(player);
        }
        else
        {
            Destroy(GameObject.Find("Player"));
        }*/
    }
}
