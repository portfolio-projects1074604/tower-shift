using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class OpenShortcut : MonoBehaviour
{
    public Tilemap shortcut;
    public GameObject screenText;

    [SerializeField] private Shortcut shortcutSO;

    private bool active;
    private void Awake()
    {
        if (shortcutSO.state)
        {
            shortcut.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (active && Input.GetButtonDown("Interact"))
        {
            shortcutSO.state = true;
            //shortcut.ClearAllTiles();
        }
        if (shortcutSO.state)
        {
            shortcut.ClearAllTiles();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            active = true;
            screenText.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            active = false;
            screenText.SetActive(false);
        }
    }
}
