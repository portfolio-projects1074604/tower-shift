using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityUpgrade : MonoBehaviour
{
    public ParticleSystem upgradeEffect;
    public Transform playerPosition;
    public AudioSource clip;

    public GameObject tutorial;

    public IEnumerator DelayedDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(tutorial);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            clip.Play();
            collision.gameObject.GetComponent<PlayerController>().AddPower("GRAVITY");
            Instantiate(upgradeEffect, playerPosition);
            tutorial.SetActive(true);
            StartCoroutine(DelayedDestroy(1.5f));
        }
    }
}
