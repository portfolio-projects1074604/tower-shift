using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTiles : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(30);
        }
    }
}
