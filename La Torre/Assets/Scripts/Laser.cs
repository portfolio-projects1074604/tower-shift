using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField] private float defDistanceRay = 100;
    [SerializeField] ParticleSystem emision;
    [SerializeField] ParticleSystem endPoint;
    [SerializeField] int orientation;

    public Transform firePoint;
    public LineRenderer m_lineRenderer;
    Transform m_transform;
    public AudioSource laser;

    private bool m_activeLaser = false;
    private bool m_calledRoutine = false;
    private void Awake()
    {
        m_transform = GetComponent<Transform>();
    }

    private void Update()
    {
        if (!m_calledRoutine)
        {
            m_calledRoutine = true;
            emision.Pause();
            emision.Clear();
            endPoint.Pause();
            endPoint.Clear();
            StartCoroutine(TurnLaser(4));
        }
        if (m_activeLaser)
        {
            emision.Play();
            endPoint.Play();
            ShootLaser();
        }
        else
        {
            Draw2DRay(firePoint.position, firePoint.position);
        }
    }

    void ShootLaser()
    {
        if (!laser.isPlaying)
        {
            laser.Play();
        }
        if (Physics2D.Raycast(m_transform.position, transform.up))
        {
            RaycastHit2D _hit;
            if (orientation == 1)
            {
                _hit = Physics2D.Raycast(m_transform.position, -transform.right);
            }
            else
            {
                _hit = Physics2D.Raycast(m_transform.position, transform.up);
            }
            Debug.Log(_hit.point);
            Draw2DRay(firePoint.position, _hit.point);
            if (_hit.collider.CompareTag("Player"))
            {
                _hit.collider.gameObject.GetComponent<PlayerController>().TakeDamage(25);
            }
        }
        else
        {
            Draw2DRay(firePoint.position, firePoint.transform.up * defDistanceRay);
        }
    }

    void Draw2DRay(Vector2 startPos, Vector2 endPos)
    {
        m_lineRenderer.SetPosition(0, startPos);
        m_lineRenderer.SetPosition(1, endPos);
    }

    private IEnumerator TurnLaser(float time)
    {
        yield return new WaitForSeconds(time);
        m_activeLaser = !m_activeLaser;
        m_calledRoutine = !m_calledRoutine;
    }
}
