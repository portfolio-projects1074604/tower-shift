using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserState : IMechaGolem
{
    MechaGolem boss;
    private bool _hasShoot = false;
    public LaserState(MechaGolem boss)
    {
        this.boss = boss;
    }

    public void GoToArmLaunchState() { }

    public void GoToIdleState()
    {
        boss.waiting = false;
        boss.currentState = boss.idleState;

    }

    public void GoToLaserState() { }

    public void GoToProtectState() { }

    public void UpdateState()
    {
        if (boss.phase == 0)
        {
            boss.WaitForAnimation(2f);
            if (!boss.waiting)
            {
                if (!_hasShoot)
                {
                    boss.animator.SetTrigger("Laser");
                    boss.laser.enabled = true;
                    _hasShoot = true;
                }
            }
            else
            {
                _hasShoot = false;
                GoToIdleState();
            }
        }
        else if (boss.phase == 1)
        {
            boss.WaitForAnimation(3f);
            if (!boss.waiting)
            {
                if (!_hasShoot)
                {
                    boss.animator.SetTrigger("Laser");
                    boss.laser.enabled = true;
                    _hasShoot = true;
                }
            }
            else
            {
                _hasShoot = false;
                GoToIdleState();
            }
        }
    }
}
