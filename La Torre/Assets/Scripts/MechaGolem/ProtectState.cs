using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectState : IMechaGolem
{

    MechaGolem boss;
    private bool _hasProtected = false;

    public ProtectState(MechaGolem boss)
    {
        this.boss = boss;
    }

    public void GoToArmLaunchState() { }

    public void GoToIdleState()
    {
        boss.waiting = false;
        boss.currentState = boss.idleState;
    }

    public void GoToLaserState() { }

    public void GoToProtectState() { }

    public void UpdateState()
    {
        boss.WaitForAnimation(3f);
        if (!boss.waiting)
        {
            if (!_hasProtected)
            {
                _hasProtected = true;
                boss.animator.SetTrigger("Protect");
            }
        }
        else
        {
            _hasProtected = false;
            GoToIdleState();
        }
    }
}
