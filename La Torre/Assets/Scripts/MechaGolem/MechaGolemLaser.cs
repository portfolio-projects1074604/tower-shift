using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechaGolemLaser : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform emision;
    public Transform end;
    public Transform end_p2;
    public Transform start;
    public AudioSource laser;

    private Vector2 original_end; 
    private Vector2 original_start;

    public LayerMask mask;
    public int phase = 0;

    // Start is called before the first frame update
    void Start()
    {
        original_end = end.position;
        original_start = start.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!laser.isPlaying)
        {
            laser.Play();
        }
        Draw2DRay(emision.position, start.position);
        RaycastHit2D _hit;
        start.position = Vector2.Lerp(start.position, end.position, 2.5f * Time.deltaTime);
        if (_hit = Physics2D.Raycast(emision.position, start.position, mask))
        {
            if (_hit.collider.CompareTag("Player"))
            {
                _hit.collider.gameObject.GetComponent<PlayerController>().TakeDamage(25);
            }
        }
        if (Vector2.Distance(start.localPosition, end.localPosition) < 0.1f)
        {
            if (phase == 0)
            {
                end.position = original_end;
                start.position = original_start;
                Draw2DRay(Vector2.zero, Vector2.zero);
                this.enabled = false;
            }
            else if (phase == 1)
            {
                end.position = end_p2.position;
                if (Vector2.Distance(start.localPosition, end.localPosition) < 0.1f)
                {
                    end.position = original_end;
                    start.position = original_start;
                    Draw2DRay(Vector2.zero, Vector2.zero);
                    this.enabled = false;
                }
            }
        }
    }

    public void ChangePhase()
    {
        phase = 1;
    }

    private void Draw2DRay(Vector2 startPos, Vector2 endPos)
    {
        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, endPos);
    }
}
