using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechaGolem : MonoBehaviour
{

    [HideInInspector] public LaserState laserState;
    [HideInInspector] public ArmLaunchState armLaunchState;
    [HideInInspector] public ProtectState protectState;
    [HideInInspector] public IdleState idleState;
    [HideInInspector] public IMechaGolem currentState;

    public Animator animator;
    public Transform launchPosition;
    public MechaGolemLaser laser;
    public Healthbar healthbar;
    public AudioSource hit;

    public bool waiting = false;
    private bool _called = false;
    public bool protecting = false;
    public int phase = 0;

    public int max_health = 500;
    public int health = 500;

    // Start is called before the first frame update
    void Start()
    {
        laserState = new LaserState(this);
        armLaunchState = new ArmLaunchState(this, launchPosition);
        protectState = new ProtectState(this);
        idleState = new IdleState(this);
        currentState = idleState;

        healthbar.SetMaxHealth(health, max_health);
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState();
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {
        if (!protecting)
        {
            hit.Play();
            health -= damage;
            healthbar.SetHealth(health);
        }
        
    }

    private void Protect()
    {
        protecting = true;
    }

    private void UnProtect()
    {
        protecting = false;
    }

    public void WaitForAnimation(float time)
    {
        if (!_called)
        {
            _called = true;
            StartCoroutine(BetweenAnimations(time));
        }
    }

    public IEnumerator BetweenAnimations(float time)
    {
        yield return new WaitForSeconds(time);
        waiting = true;
        _called = false;
    }
}
