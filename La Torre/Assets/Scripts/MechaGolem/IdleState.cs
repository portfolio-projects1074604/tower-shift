using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IMechaGolem
{
    MechaGolem boss;

    private bool has_launched = false;
    private bool has_protected = false;
    private bool has_lasered = false;

    public IdleState(MechaGolem boss)
    {
        this.boss = boss;
    }

    public void GoToArmLaunchState()
    {
        boss.waiting = false;
        boss.currentState = boss.armLaunchState;
    }

    public void GoToIdleState() { }

    public void GoToLaserState()
    {
        boss.waiting = false;
        boss.currentState = boss.laserState;
    }

    public void GoToProtectState()
    {
        boss.waiting = false;
        boss.currentState = boss.protectState;
    }

    public void UpdateState() 
    {
        boss.WaitForAnimation(3f);
        if (boss.waiting)
        {
            if (!has_launched)
            {
                has_launched = true;
                has_protected = false;
                GoToArmLaunchState();
            }
            else if (!has_lasered)
            {
                has_lasered = true;
                GoToLaserState();
            }
            else if (!has_protected)
            {
                has_protected = true;
                has_launched = false;
                has_lasered = false;
                GoToProtectState();
            }
        }
    }
}
