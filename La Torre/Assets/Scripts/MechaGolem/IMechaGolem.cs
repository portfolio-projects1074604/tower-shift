using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMechaGolem
{
    void UpdateState();

    void GoToLaserState();

    void GoToArmLaunchState();

    void GoToProtectState();

    void GoToIdleState();

}
