using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmProjectile : MonoBehaviour
{
    private Transform target;
    private Vector2 target_position;
    private float speed = 2f;


    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("CeilingCheck").transform;
        target_position = new Vector2(target.position.x, target.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = Vector2.Lerp(transform.position, target_position, Time.deltaTime * speed);
        if (Vector2.Distance(transform.position, target_position) < 0.1f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(30);
            Destroy(gameObject);
        }
        if (collision.CompareTag("Terrain"))
        {
            Destroy(gameObject);
        }
    }
}
