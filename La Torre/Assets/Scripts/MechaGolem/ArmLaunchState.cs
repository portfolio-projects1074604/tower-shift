using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmLaunchState : IMechaGolem
{
    MechaGolem boss;
    Transform launchPosition;
    GameObject arm_prefab;
    GameObject instance;
    private bool _hasLaunched = false;

    public ArmLaunchState(MechaGolem boss, Transform launchPosition)
    {
        this.boss = boss;
        this.launchPosition = launchPosition;
        arm_prefab = Resources.Load("arm_projectile") as GameObject;
    }

    public void GoToArmLaunchState() { }

    public void GoToIdleState()
    {
        boss.waiting = false;
        boss.currentState = boss.idleState;
    }

    public void GoToLaserState() { }

    public void GoToProtectState() { }

    public void UpdateState()
    {
        boss.WaitForAnimation(2f);
        if (!boss.waiting)
        {
            if (!_hasLaunched)
            {
                _hasLaunched = true;
                boss.animator.SetTrigger("Launch");
                instance = Object.Instantiate(arm_prefab, launchPosition.position, launchPosition.rotation);
            }
        }
        else
        {
            _hasLaunched = false;
            GoToIdleState();
        }
        
    }
}
