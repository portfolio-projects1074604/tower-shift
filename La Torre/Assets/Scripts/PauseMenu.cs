using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public Canvas pausemenu;
    public GameObject player;

    /*public void ShowMenu()
    {
        player.GetComponent<PlayerController>().enabled = false;
    }*/

    public void HideMenu()
    {
        player.GetComponent<PlayerController>().enabled = true;
        this.gameObject.SetActive(false);
    }

    public void ReturntoMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
