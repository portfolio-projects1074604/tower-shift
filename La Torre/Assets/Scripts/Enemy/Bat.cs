using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{

    public AudioSource hit;

    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float magnitude = 0.5f;
    [SerializeField] private float frequency = 20f;

    public bool facingRight;

    private Vector3 pos;

    public Animator animator;
    public int health;



    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health > 0)
        {
            if (facingRight)
            {
                MoveRight();
            }
            else
            {
                MoveLeft();
            }
        }
        else if (health <= 0)
        {
            animator.SetTrigger("Dead");
        }
    }

    public void TakeDamage(int damage)
    {
        hit.Play();
        health -= damage;
    }

    public void Finish()
    {
        Destroy(gameObject);
    }

    private void MoveRight()
    {
        pos += transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
    private void MoveLeft()
    {
        pos -= transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }

}
