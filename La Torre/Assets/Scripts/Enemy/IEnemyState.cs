using UnityEngine;

public interface IEnemyState
{
    void UpdateState();

    void GoToPatrolState();

    void GoToAttackState();


    void OnTriggerEnter2D(Collider2D col);

    void OnCollisionEnter2D(Collision2D col);
}
