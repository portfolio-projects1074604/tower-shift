using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState
{
    EnemyAI enemy;
    Transform target;
    private float speed = 2f;
    private int damage = 15;

    private bool _has_flipped = false;

    public AttackState(EnemyAI enemyAI)
    {
        enemy = enemyAI;
        target = GameObject.Find("Player").transform;
    }

    public void GoToAttackState() { }

    public void GoToPatrolState()
    {
        enemy.currentState = enemy.patrolState;
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            col.gameObject.GetComponent<PlayerController>().TakeDamage(damage);
            enemy.DestroyEnemy();
        }
    }

    public void OnTriggerEnter2D(Collider2D col) { }

    public void UpdateState()
    {
        enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, target.position, speed * Time.deltaTime);
        if (_has_flipped && enemy.transform.position.x > target.position.x)
        {
            Vector2 scale = enemy.transform.localScale;
            scale.x *= -1;
            enemy.transform.localScale = scale;
            _has_flipped = false;
        }
        if (!_has_flipped && enemy.transform.position.x < target.position.x)
        {
            Vector2 scale = enemy.transform.localScale;
            scale.x *= -1;
            enemy.transform.localScale = scale;
            _has_flipped = true;
        }
    }

}
