using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState
{
    EnemyAI enemy;

    public PatrolState(EnemyAI enemyAI)
    {
        enemy = enemyAI;
    }

    public void GoToAttackState()
    {
        enemy.currentState = enemy.attackState;
    }

    public void GoToPatrolState() { }

    public void OnCollisionEnter2D(Collision2D col) { }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            GoToAttackState();
        }
    }

    public void UpdateState()
    {
        
    }

}
