using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour
{

    public Animator animator;
    public Transform target;
    public int health;
    public AudioSource hit;

    private float speed = 3f;

    private bool _inRange;
    private bool _has_to_walk = true;
    private bool _has_flipped = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_inRange && _has_to_walk)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);


            if (!_has_flipped && transform.position.x > target.position.x)
            {
                Vector2 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
                _has_flipped = true;
            }
            if (_has_flipped && transform.position.x < target.position.x)
            {
                Vector2 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
                _has_flipped = false;
            }
        }
        if (health <= 0)
        {
            animator.SetTrigger("Dead");
        }
    }

    public void Attack()
    {
        target.GetComponent<PlayerController>().TakeDamage(15);
    }

    public void TakeDamage(int damage)
    {
        hit.Play();
        health -= damage;
    }

    public void Finish()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            animator.SetTrigger("Walk");
            _inRange = true;
            target = collision.gameObject.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _inRange = false;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _has_to_walk = false;
            animator.SetTrigger("Attack");
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _has_to_walk = true;
        }
    }
}
