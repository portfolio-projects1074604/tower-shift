using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject target;
    public GameObject bullet;
    public Transform shootPoint;
    public AudioSource laserShoot;

    public float speed;
    public float RotationModifier;
    public int health;

    private bool shoot = true;

    // Start is called before the first frame update
    void Start()
    {
        bullet = Resources.Load("Bullet") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            Vector2 vectorToTarget = target.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - RotationModifier;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);

            if (shoot && target.transform.position.y <= transform.position.y)
            {
                laserShoot.Play();
                Instantiate(bullet, shootPoint.position, shootPoint.rotation);
                shoot = false;
                StartCoroutine(ShootCooldown());
            }
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }

    public IEnumerator ShootCooldown()
    {
        yield return new WaitForSeconds(1.5f);
        shoot = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            target = collision.gameObject;
        }
    }
}
