using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float speed = 4f;
    // Update is called once per frame
    void Update()
    {
        transform.position += speed * Time.deltaTime * -transform.up;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(15);
        }
        if (!collision.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
    }
}
