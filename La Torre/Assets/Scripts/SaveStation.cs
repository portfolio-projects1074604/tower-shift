using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveStation : MonoBehaviour
{
    public GameObject savePrompt;

    public void Save()
    {
        var player = GameObject.Find("Player").GetComponent<PlayerController>();
        player.last_SR = SceneManager.GetActiveScene().name;
        SaveSystem.SavePlayer(player);
        savePrompt.SetActive(false);
    }

    public void Back()
    {
        savePrompt.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            savePrompt.SetActive(true);
        }
    }
}
