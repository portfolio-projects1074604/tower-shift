using UnityEngine;


[CreateAssetMenu(menuName = "Levels/Shortcut")]

public class Shortcut : ScriptableObject
{
    public bool state = false;
}
